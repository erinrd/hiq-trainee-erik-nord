const express = require('express')
const formidable = require('formidable')
const fs = require('fs')
const fooBarProcess = require('./wordCounter').fooBarProcess
const mostFrequentWord = require('./wordCounter').mostFrequentWord
const path = require('path')

/*
 * NodeJS code
 */
function findMatchReplace(buffer) {
	const rawData = buffer.toString('utf-8', 0, buffer.length)
	const lines = rawData.split('\n')
	const common = mostFrequentWord(lines)
	if(common.toString().length == 0) {
		return fooBarProcess(NaN, lines)
	}
	return fooBarProcess(common, lines)
}

/*
 * Server code
 */
const app = express()
app.use(express.static('public')) // to get index.html
app.listen(3000, () => console.log('Running on port 3000...'))

/*
 * Template function to take html headers from singleton file
 */
function createTemplate(template, content, callback) {
	fs.readFile('./public/' + template + '.html', function(err, data) {
		callback(data + content + '</div>\n</body>\n</html>\n')
	})
}

/*
 * Listen for upload
 */
app.post('/upload.html', function(req, res) {
	new formidable.IncomingForm().parse(req, function(err, fields, files) {
		// file is the name of the <input> field of type file
		const path = files.file.path
		fs.readFile(path, function(err, data) {
			console.time('Total time to process file')
			const result = findMatchReplace(data)
			console.timeEnd('Total time to process file')
			res.set('Content-Type', 'text/html')
			if(err) {
				createTemplate('error', '<p>Unknown error occured!</p>', function(template) {
					res.send(template)
				})
			} else if (result.length == 0) {
				createTemplate('error', '<p>Input file was empty! Please select a non-empty file</p>', function(template) {
					res.send(template)
				})
			} else {
				createTemplate('result', result, function(template) {
					res.send(template)
				})
			}
		})
	})
})
