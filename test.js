import test from 'ava'
import { isWord, trimWord, mostFrequentWord, fooBarProcess } from './wordCounter'
import { linkedList } from './linkedList'

/*
 * Unit tests for isWord functuon. Test case titles are self-explanatory
 */

test('isWord: empty string is not a word', t => {
	t.false(isWord(''))
})

test('isWord: word is a word', t => {
	t.true(isWord('trainee'))
})


test('isWord: trailing special characters is word', t => {
	t.true(isWord('trainee!!!'))
})


test('isWord: leading special characters is no word', t => {
	t.false(isWord('...trainee'))
})

test('isWord: digits only is not a word', t => {
	t.false(isWord('1337'))
})

test('isWord: digits and characters is a word', t => {
	t.true(isWord('h3ll0'))
	t.true(isWord('4h3ll0'))
	t.true(isWord('h3ll0!!!'))
	t.false(isWord('...h3ll0!!!'))
})

/*
 * Unit tests for trim function. Test case titles are self-explanatory
 */

test('trimWord: empty string trims to empty string', t => {
	t.is(trimWord(''), '')
})

test('trimWord: word trims to word', t => {
	t.is(trimWord('trainee'), 'trainee')
	t.is(trimWord('h3ll0'), 'h3ll0')
})


test('trimWord: leading special chars dont care (not a word)', t => {
	t.is(trimWord('!:;,trainee'), '')
	t.is(trimWord('\'?.4life'), '')
})


test('trimWord: trailing special chars', t => {
	t.is(trimWord('trainee!:;,'), 'trainee')
	t.is(trimWord('4life\'?.'), '4life')
})

/*
 * Unit test for mostFrequentWord every case is one test. Test case titles are self-explanatory
 */


test('mostFrequentWord: empty text has no common word', t => {
	t.is(mostFrequentWord([]), '')
})

test('mostFrequentWord: normal case scenario', t => {
	const power = ['With great power', 'comes great responsibility']
	t.is(mostFrequentWord(power), 'great')
})

test('mostFrequentWord: different char cases scenario', t => {
	const potatoes = ['You could say potaTOES', 'or you could say POtatoes...', 'but stick with poTAtoes!']
	t.is(mostFrequentWord(potatoes), 'potatoes')
})


test('mostFrequentWord: trailing special chars still count as same word', t => {
	const milk = ['Milk, makes you strong', 'I love milk!', 'Got milk?']
	t.is(mostFrequentWord(milk), 'milk')
})

/*
 * Unit test for fooBarProcess every case is one test. Test case titles are self-explanatory
 */

test('fooBarProcess: empty line yields empty line', t => {
	t.is(fooBarProcess('', []), '')
})

test('fooBarProcess: single line replacing word \'time\'', t => {
	const line = ['Once upon a time there was a kid and his name was Morty. He had a crazy grandpa named Rick.']
	t.is(fooBarProcess('time', line),
		'Once upon a footimebar there was a kid and his name was Morty. He had a crazy grandpa named Rick.')
})

test('fooBarProcess: two lines, replacing word \'you\'', t => {
	const first = 'Haven\'t you heard?'
	const second = 'What, about the bird?'
	t.is(fooBarProcess('you', [first, second]),
		'Haven\'t fooyoubar heard?<br />\nWhat, about the bird?')
})


test('fooBarProcess: several lines, replacing word \'test\'', t => {
	const first = 'This test is'
	const second = 'the last TEST'
	const third = 'of all tests'
	t.is(fooBarProcess('test', [first, second, third]),
		'This footestbar is<br />\nthe last fooTESTbar<br />\nof all tests')
})

/*
 * This test is estethics!
 */
test.todo('Trailing special characters gives ugly transformation')

/*
 * Unit tests for the linkedList data structure.
 * Did not end up using it in final, but leaving the code
 * because it took time to create!
 */

/*
 * Test the constructor for both a declared call and empty call
 */
test('linkedList: constructor', t => {
	const empty = new linkedList()
	const declared = new linkedList('HiQ')
	t.true(empty.head === null)
	t.true(empty.length === 0)
	t.true(declared.head != null)
	t.true(declared.head.element === 'HiQ')
	t.true(declared.length === 1)
})

/*
 * Test the add method and makes sure length and head gets updated
 */
test('linkedList add', t => {
	const list = new linkedList()
	t.true(list.length === 0)
	list.add('HiQ')
	t.true(list.head.element === 'HiQ')
	t.true(list.length === 1)
	list.add('')
	t.true(list.head.element === '')
	t.true(list.length === 2)
})

/*
 * Test that addLast updates the head pointer properly
 */
test('linkedList addLast yields head', t => {
	const list = new linkedList()
	list.addLast('HEAD')
	list.addLast('TAIL')
	t.true(list.head != null)
	t.true(list.head.element === 'HEAD')
})


/*
 * Test that addLast increases length and tail pointer
 */
test('linkedList addLast from empty', t => {
	const list = new linkedList()
	list.addLast('Chicken')
	t.true(list.tail.element === 'Chicken')
	t.true(list.length === 1)
	list.addLast('Egg')
	t.true(list.tail.element === 'Egg')
	t.true(list.length === 2)
})


/*
 * Test that addLast increase length and tail pointer
 * from a list that was constructed with argument
 */
test('linkedList addLast from non-empty', t => {
	const emptyList = new linkedList('Human')
	emptyList.addLast('Woman')
	t.true(emptyList.tail.element === 'Woman')
	t.true(emptyList.length === 2)
	emptyList.addLast('Man')
	t.true(emptyList.tail.element === 'Man')
	t.true(emptyList.length === 3)
})


/*
 * Test that pop decreases length and head pointer
 */
test('linkedList pop', t => {
	const list = new linkedList('trip')
	const pop = list.pop()
	t.true(list.head === null)
	t.true('trip' == pop)
	t.true(list.length === 0)
})


/*
 * Test makes sure that added elements can get poped
 */
test('linkedList add then pop', t => {
	const list = new linkedList()
	list.add('Rock\'n\'Roll')
	const rock = list.pop()
	t.true(rock === 'Rock\'n\'Roll')
	t.true(list.head === null)
	t.true(list.length === 0)
})


/*
 * Testing the toString function for Strings. I only need the list
 * to work with strings for this project.
 */
test('linkedList toString', t => {
	const list = new linkedList()
	list.addLast('do')
	list.addLast('re')
	list.addLast('mi')
	list.addLast('fa')
	list.addLast('so')
	t.is(list.toString(),'doremifaso')
})


/* END OF TESTS */
