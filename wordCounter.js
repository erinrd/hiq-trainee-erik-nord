const { linkedList } = require('./linkedList.js')
const SPEC_REGEX = new RegExp('[,.!?;:\']+')
const WORD_REGEX = new RegExp('[a-zA-Z0-9åäöÅÄÖ]*[a-zA-ZåäöÅÄÖ]+[a-zA-Z0-9åäöÅÄÖ]*[,.!?;:]*')

/*
 * find out if parameter "word" has the pattern of a word.
 * returns result true or false
 * example: "hiq.word" is not word while "hiqword" is
 */
function isWord(word) {
	return WORD_REGEX.exec(word) == word
}

/*
 * removes leading and trailing [,.-_!?''] returns the string
 */
function trimWord(word) {
	return word.split(SPEC_REGEX)[0]
}

/*
 * takes a collection of lines and returns the most frequent word
 * according to my own benchmark with two different machines
 * forEach loops are faster in this use case!
 */
function mostFrequentWord(lines) {
	const map = new Map() // Map with (String, Int) key-value pairs
	let result = ''
	let highest = 0
	lines.forEach(function(line) {
		// for every line, find words
		const words = line.split(' ')
		words.forEach(function(word) {
			// for every potential word check if
			// it matches my definition of a word
			if(isWord(word)) {
				// make it lowercase before mapping
				const lower = trimWord(word.toLowerCase())
				if(!map.has(lower)) {
					// new map entry
					map.set(lower, 1)
					if(highest < 1) {
						highest = 1
						result = lower
					}
				} else {
					// old map entry
					const value = map.get(lower) + 1
					map.set(lower, value)
					if(value > highest) {
						result = lower
						highest = value
					}
				}
			}
		})
	})
	return result
}

/*
 * takes string pattern and collection of strings and
 * returns a string where every occurance of "pattern"
 * is surrounded with foo + pattern + bar
 */
function fooBarProcess(pattern, lines) {
	const array = []
	lines.forEach(function(line) {
		const words = line.split(' ')
		words.forEach(function(word) {
			const lower = trimWord(word.toLowerCase())
			if(lower === pattern) {
				array.push('foo' + word + 'bar ')
			} else {
				array.push(word + ' ')
			}
		})
		array[array.length - 1] = array[array.length - 1].trim()
		array.push('<br />\n')
	})
	if(array != null) {
		return array.join('').slice(0,-7) // removes last <br>
	} else {
		return ''
	}
}

module.exports = {
	isWord: isWord,
	trimWord: trimWord,
	mostFrequentWord: mostFrequentWord,
	fooBarProcess: fooBarProcess
}
