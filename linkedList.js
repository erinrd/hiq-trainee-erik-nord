/*
 * A linked list data structure implemented from scratch
 * I'ts a single linked list with a tail pointer for
 * constant time insertion through appending.
 * Does not contain all conventional methods because
 * I only implemented those that I needed for this project.
 */

class linkedList {

	/*
	 * Creates a new linked list with and adds the
	 * element element to it. Argument is optional.
	 */
	constructor(element) {
		this.head = null
		this.length = 0
		this.tail = this.head
		this.element = null
		if(!(element === undefined)) {
			this.tail = this.add(element)
		}
	}

	/*
	 * Adds an element at the head of the linked list.
	 */
	add(element) {
		const node = { element }
		node.next = this.head
		this.head = node
		this.length++
		return this
	}

	/*
	 * Adds an element at the tail of the linked list.
	 */
	addLast(element) {
		const node = { element }
		if(this.length > 0) {
			this.tail.next = node
		} else {
			this.head = node
		}
		this.tail = node
		this.length++
		return this
	}

	/*
	 * Removes and returns the element at the head.
	 */
	pop() {
		if(this.length === 0) {
			return undefined
		}

		const e = this.head.element
		this.head = this.head.next
		this.length--

		return e
	}

	/*
	 * Returns a string representation concated toghether
	 * in a single string.
	 */
	toString() {
		const array = []
		const len = this.length
		for(var i = 0; i < len; i++) {
			array.push(this.pop())
		}
		return array.join('')
	}
}

module.exports =  {
	linkedList: linkedList
}
