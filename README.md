# README #

## HiQ Trainee code case by Erik Nord ##

My solution is complete with a basic backend. I'm using node.js and express for this project. The reason for this is that I have never made a web app before and it was recommended to me that I should use node with express for a straight forward way to accomplish a simple demo of back end development using JavaScript as the only language. As the problem was simple in its nature I wanted a simple and developer friendly way to approach it.

To manage my packages i use npm, because it works so well with the node/express enviroment. It was very easy to manage and organize the packages. I did little to no configuration of the modules.

As for how I solved the problem; I first solved it in Java as a prototype to check if I could find a solution I was happy with. Then - to make sure no functionality was lost in translation, I implemented the JavaScript in a Test Driven way. For this I used AVA because i found the documentation to be very easy to grasp.

Early on, I found that there was no easy to use IO within JavaScript. I have never touched JavaScript and falsely assumed that one could read file from disk like any other language i've used. To tackle this I took inspiration from [paambaati on GitHub](https://gist.github.com/paambaati/db2df71d80f20c10857d) where he put up a simple code demo of how to read files using HTTP requests in node enviroment. After some prototyping in JavaScript to make sure I can use the file reading demo in a meaningful way, I settled and started writing my tests...

When I felt pretty much done with the back-end I realized how slow the parser was. Being new to JavaScript this was a challange. I found that the reason for this was because strings seem to concatenate in linear time but I need it to be in constant time. I tried two different ways to trim down execution time. First I made a linked list data structure in order to get constant insertion. This was very very fast. But further research made me realzie that if I concatenate strings by Array.push() ing all substrings and then Array.join("") them, I can get constant insertion time. This was just a little bit faster than my linked list so I ended up using Array.push(). I still take pride that I managed to create a fully working linkedList in JS using TDD. The execution time was 15% slower (which is 200ms diff for the Sherlocked in-file running on my laptop) compared to push() and pop().

TL;DR:  
* enviroment:  
    * npm - easy to manage packages  
    * node - easy to get started with  
    * express - works good in npm node.js eviroment  
    * AVA - to use TDD for the Java to JavaScript conversion  
* the JavaScript - only language in this repository  
    * wordCounter.js is a module for counting and processing words  
    * linkedList.js is a linked list implemented from scratch. However I did not end up using it in the final commit  

## How to install and run ##
Download the zip, unzip in a new folder and run "npm install" in that folder. To start the web server, run "node app.js"

### What can still be done? ###
The only problem my procedure currently has is that when the most common word occurs with trailing special characters the print will be of the pattern fooWORD[,.!?;:\']+bar but fooWRORDbar[,.!?;:\']+ would be of preference.
